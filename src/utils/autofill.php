<?php
    namespace Utils;
    use Upgrade\Capitulo as Capitulo;
    use Upgrade\Serie as Serie;
    use Upgrade\Temporada as Temporada;


    class Autofill {
        private $series = [];

        private function f_rand($min=0,$max=1,$mul=1000000){
            if ($min>$max) return false;
            return mt_rand($min*$mul,$max*$mul)/$mul;
        }

        public function __construct() {        
            
        }

        public function fill() {
            $serie = new Serie("Juego de Tronos", "Aventuras");
            $temporada1 = new Temporada(1, '2005');
            $temporada2 = new Temporada(2, '2006');
            $temporada3 = new Temporada(3, '2007');
            $temporada4 = new Temporada(3, '2008');
            $temporada5 = new Temporada(3, '2009');
            $temporada6 = new Temporada(3, '2010');
        
            for ($num = 0; $num <10; $num++) {
                $chapter = new Capitulo(120.0, "capitulo", "10/10/2007", $this->f_rand(0.5, 10, 2), $num + 1);
                $temporada1->addChapter($chapter);
                $temporada2->addChapter($chapter);
                $temporada3->addChapter($chapter);
                $temporada4->addChapter($chapter);
                $temporada5->addChapter($chapter);
                $temporada6->addChapter($chapter);
            }
        
            $serie->addSeason($temporada1);
            $serie->addSeason($temporada2);
            $serie->addSeason($temporada3);
            array_push($this->series, $serie);

            $serie = new Serie("Los Soprano", "Crimen");
            $temporada1 = new Temporada(1, '2000');
            $temporada2 = new Temporada(2, '2001');
            $temporada3 = new Temporada(3, '2002');
            $temporada4 = new Temporada(3, '2003');
            $temporada5 = new Temporada(3, '2004');
            $temporada6 = new Temporada(3, '2005');
        
            for ($num = 0; $num <10; $num++) {
                $chapter = new Capitulo(120.0, "capitulo", "10/10/2000", $this->f_rand(0.5, 10, 2), $num + 1);
                $temporada1->addChapter($chapter);
                $temporada2->addChapter($chapter);
                $temporada3->addChapter($chapter);
                $temporada4->addChapter($chapter);
                $temporada5->addChapter($chapter);
                $temporada6->addChapter($chapter);
            }
        
            $serie->addSeason($temporada1);
            $serie->addSeason($temporada2);
            $serie->addSeason($temporada3);
            array_push($this->series, $serie);

            $serie = new Serie("The Wire", "Policiaca");
            $temporada1 = new Temporada(1, '1995');
            $temporada2 = new Temporada(2, '1996');
            $temporada3 = new Temporada(3, '1997');
            $temporada4 = new Temporada(3, '1998');
            $temporada5 = new Temporada(3, '1999');
            $temporada6 = new Temporada(3, '2000');
        
            for ($num = 0; $num <10; $num++) {
                $chapter = new Capitulo(120.0, "capitulo", "10/10/1995", $this->f_rand(0.5, 10, 2), $num + 1);
                $temporada1->addChapter($chapter);
                $temporada2->addChapter($chapter);
                $temporada3->addChapter($chapter);
                $temporada4->addChapter($chapter);
                $temporada5->addChapter($chapter);
                $temporada6->addChapter($chapter);
            }
        
            $serie->addSeason($temporada1);
            $serie->addSeason($temporada2);
            $serie->addSeason($temporada3);
            array_push($this->series, $serie);
            return $this->series;
        }  
    }
?>