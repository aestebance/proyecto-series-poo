# Práctica de series - temporadas - capítulos

### Ejecución del contenedor
Disponemos de un fichero _docker-compose.yml_ con el que levantar el contenedor y al mismo tiempo creará la imagen de Docker.
Lo primero que tenemos que hacer es crear una carpeta vacía que se llame **db-data**.
Antes de levantar el contenedor tenemos que modificar el fichero _docker-compose.yml_ y sustituir los valores de user y uid por los que correspondan.
En el fichero de ejemplo están kiko y 1000
**nota:** también se utiliza el valor 1000 para decirle al servidor Apache que se ejcute con dicho id.
El valor 1000 es id por defecto que se crea (en distribuciones Linux como por ejemplo Ubuntu), para el primer usuario, por lo tanto es posible que
os sirva, lo único que tenéis que hacer es es cambiar el nombre de usuario por el de vuestro usuario en vuestra máquina.
Para saber el uid y el nombre de usuario ejecutar lo siguiente:

```
id
```
dando como resultado algo parecido a esto:
 ```
uid=1000(kiko) gid=1000(kiko) groups=1000(kiko),4(adm).......
```

Levantamos el contenedor con (debemos estar en la carpeta del proyecto):
```
docker-compose up -d
```

Una vez levantado, podemos asegurarnos que está todo correcto ejecutando:

```
docker-compose ps
```

Para 'entrar' en el contenedor utilizaremos la opción **-u** para indicar el usuario creado anteriormente:

```
docker-compose exec -u kiko webserver bash
```

Finalmente sólo nos queda instalar las dependencias del proyecto:

```shell script
composer install
```

Una vez hecho esto, en la URL http://localhost:8080 tendremos nuestra aplicación
recién instalada.


### Conocimientos previos de POO
- Clases y objetos
- Visibilidad
- Herencia
- Clases abstractas (opcional)
- Autoload y namespaces
- Paso por referencia (opcional, se puede pasar la variable por valor)

### Descripción del proyecto
Para este proyecto se ha facilitado la estructura de carpetas y archivos para poder completarlo,
en la carpeta classes están las clases a crear y luego en la raíz el index.php. Además se ha
añadido una clase llamada Autofill que se encuentra en la carpeta utils con un único método público
llamado fill() que al ser llamado devuelve un array de series con varios objetos de tipo serie, con
temporadas dentro y estas a su vez con capítulos, para facilitar uso. Para poder usar esta utilidad
será necesario respetar el nombre de los métodos que se pueden encontrar en el archivo autofill.php,
ya que en caso contrario no funcionará.

El proyecto se basa en 4 modelos (Capítulo, ElementoMultimedia, Serie y Temporada) del
ejercicio de las slices. Dispone de una única vista donde se muestran las series, temporadas y
capítulos. Además podemos incluir un botón de autorellenar que llame a la clase Autofill y
rellene los datos.

Los requisitos mínimos de esta práctica son los siguientes:
- Crear las clases con sus propiedades y métodos, así como las herencias, si las hay.
- Crear una vista en el index.php que muestre los datos de la siguiente forma:
- serie
    * Temporadas
        + Capítulos
    
Además se deberá mostrar sus propiedades (año, valoración, etc).

Hint: Utilizar la clase Autofill para rellenar los datos a mostrar.

Los requisitos opcionales aunque recomendables son los siguientes:
- Mostrar en la vista la valoración media de cada serie y temporada.
- Hacer que los datos sean permanentes guardándolos previamente en un fichero.
- Añadir un botón borrar que permita borrar un capítulo, temporada y/o serie (Obviamente, si 
  se borra una serie se deberán borrar también sus temporadas y capítulos, y con temporada 
  exactamente lo mismo). Para hacer este paso se deberá haber completado el anterior.
- Aplicarle estilo a la vista (bootstrap sería buena opción)

### Estructura de clases
Se recomienda que las propiedades sean privadas y/o protected y crear sus getters setters, salvo que
expresamente queramos que no se pueda acceder a esa propiedad desde fuera de la clase. Los métodos públicos
y privados a crear queda a libertad del programador. Aunque, como se ha dicho anteriormente, si queremos
facilitar el trabajo y que nos auto rellene los datos la clase Autofill deberemos respetar, al menos,
los métodos públicos a los que llama esa clase. Aunque también podemos crear nuestro propio autofill o
crear los objetos en el mismo index.php.

### Namespaces
Como se puede observar en el fichero composer.json se han creado dos namespace, uno denominado "Upgrade"
que hace referencia a la carpeta /src/classes y otro denominado "Utils" que hace referencia a la carpeta
"/src/utils". Además todos los archivos facilitados ya tienen establecido su namespace al inicio del mismo.
Para facilitar el trabajo en el index.php se han incluido los require de todas las clases (se puede hacer 
uso de todas ellas o no).

TIP: Se recomienda crear una constante en el index.php que contenga la ruta dónde se van a guardar los datos
en el caso de querer realizar esa tarea opcional. Los datos se pueden almacenar en cualquier fichero dentro de /src
ya que, aunque es algo no recomendable ya que es una ruta accesible desde el exterior y, por lo tanto, vulnerable,
cuando empecemos con bases de datos no será necesario almacenar en ficheros.
